package SCDias;

import java.util.Date;

public class DmrUser implements Comparable {
    private int Order;
    private Date LastHeard;
    private int UserID;
    private String UserCallsign;
    private String UserName;
    private int RepeaterID;
    private String RepeaterCallsign;
    private String RepeaterLocation;

    public DmrUser(int order, Date lastHeard, int userID, String userCallsign, String userName, int repeaterID, String repeaterCallsign, String repeaterLocation) {
        Order = order;
        LastHeard = lastHeard;
        UserID = userID;
        UserCallsign = userCallsign;
        UserName = userName;
        RepeaterID = repeaterID;
        RepeaterCallsign = repeaterCallsign;
        RepeaterLocation = repeaterLocation;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public Date getLastHeard() {
        return LastHeard;
    }

    public void setLastHeard(Date lastHeard) {
        LastHeard = lastHeard;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getUserCallsign() {
        return UserCallsign;
    }

    public void setUserCallsign(String userCallsign) {
        UserCallsign = userCallsign;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getRepeaterID() {
        return RepeaterID;
    }

    public void setRepeaterID(int repeaterID) {
        RepeaterID = repeaterID;
    }

    public String getRepeaterCallsign() {
        return RepeaterCallsign;
    }

    public void setRepeaterCallsign(String repeaterCallsign) {
        RepeaterCallsign = repeaterCallsign;
    }

    public String getRepeaterLocation() {
        return RepeaterLocation;
    }

    public void setRepeaterLocation(String repeaterLocation) {
        RepeaterLocation = repeaterLocation;
    }

    @Override
    public String toString() {
        return String.format("%s\t%s\t%s", getLastHeard().toString(), getUserCallsign(), getUserName());
    }

    @Override
    public int compareTo(Object o) {
        return this.getLastHeard().compareTo(((DmrUser) o).getLastHeard());
    }

    public String toBaofengDM5R() {
        return String.format("%s %s,%s,Private Call,Off,None", getUserCallsign(), getUserName(), getUserID());
    }
}