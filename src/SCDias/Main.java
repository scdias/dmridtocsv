package SCDias;

import SCDias.UI.FrmPrincipal;
import com.alee.laf.WebLookAndFeel;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                WebLookAndFeel.install();
                FrmPrincipal.main();
            }
        });
    }
}
