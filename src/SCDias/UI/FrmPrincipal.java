package SCDias.UI;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.text.WebTextField;
import com.alee.managers.tooltip.TooltipWay;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static SCDias.Utils.Utils.ExportBaofengDM5R;

public class FrmPrincipal {
    private JPanel rootPanel;
    private JComboBox cbox_radioModel;
    private JTextField txt_startPos;
    private JTextField txt_endPos;
    private JTextField txt_path;
    private JButton btn_browse;
    private JButton btn_generate;

    public FrmPrincipal() {
        btn_browse.addActionListener(actionEvent -> {
            File file = WebDirectoryChooser.showDialog(null, "Escolha o diretório para exportar os DMR IDs");
            if (file == null) return;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String date = dateFormat.format(new Date());
            txt_path.setText(String.format("%s/%s_userlist.csv", file.getAbsolutePath(), date));
        });
        btn_generate.addActionListener(actionEvent -> {
            btn_generate.setEnabled(false);
            try {
                ExportBaofengDM5R(txt_path.getText(), Integer.parseInt(txt_startPos.getText()), Integer.parseInt(txt_endPos.getText()));
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }
            btn_generate.setEnabled(true);
        });
    }

    public static void main() {
        JFrame frame = new JFrame("DmrId to CSV");
        frame.setContentPane(new FrmPrincipal().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        URL iconURL = FrmPrincipal.class.getResource("/SCDias/Res/iconfinder_radio-tower_298855.png");
        ImageIcon icon = new ImageIcon(iconURL);
        frame.setIconImage(icon.getImage());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void createUIComponents() {
        cbox_radioModel = new WebComboBox();
        cbox_radioModel.addItem("Baofeng DM-5R");
        ((WebComboBox) cbox_radioModel).setToolTip("Escolha o modelo do rádio", TooltipWay.down, 0);
        btn_browse = new WebButton();
        txt_path = new WebTextField();
        ((WebTextField) txt_path).setInputPrompt("Escolha o caminho do ficheiro csv....");
    }
}
