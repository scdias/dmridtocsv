package SCDias.Utils;

import SCDias.DmrUser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Utils {
    private static final String URL = "https://ham-digital.org/dmr-user.php?n=100&s=%d&from_id=268";

    private static String getPage(int page) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(String.format(URL, page * 100));
        Response response = target.request(MediaType.TEXT_HTML).get(Response.class);
        if (response.getStatus() != 200) {
            System.err.println(response.getStatus() + " " + response.getStatusInfo() +
                    "\n" + response.readEntity(String.class));
            throw new RuntimeException("Erro ao ligar ao servidor");
        }
        return response.readEntity(String.class);
    }

    private static ArrayList<DmrUser> parseTable() throws ParseException {
        int page = 0;
        int records;
        ArrayList<DmrUser> dmrUsers = new ArrayList<>();
        do {
            Document doc = Jsoup.parse(getPage(page++));
            Element table = doc.select("table").get(0);
            Elements rows = table.select("tr");

            for (Element row : rows) {
                Elements cols = row.select("td");
                if (cols.size() != 0) {
                    dmrUsers.add(new DmrUser(
                            Integer.parseInt(cols.get(0).text()),
                            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cols.get(1).text()),
                            Integer.parseInt(cols.get(2).text()),
                            cols.get(3).text(),
                            cols.get(4).text(),
                            Integer.parseInt(cols.get(5).text()),
                            cols.get(6).text(),
                            cols.get(7).text()
                    ));
                }
            }
            records = rows.size();
        } while (records != 1);
        return dmrUsers;
    }

    public static void ExportBaofengDM5R(String filename, int startPos, int endPos) throws ParseException, IOException {
        ArrayList<DmrUser> dmrUsers = parseTable();
        FileWriter writer = new FileWriter(filename);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        if (dmrUsers.size() == 0) return;
        bufferedWriter.write("Number,Name,Call ID,Type,Ring Style,Call Receive Tone");
        bufferedWriter.newLine();

        dmrUsers.sort(Collections.reverseOrder());

        for (DmrUser dmrUser : dmrUsers) {
            bufferedWriter.write(String.format("%s,%s", startPos++, dmrUser.toBaofengDM5R()));
            bufferedWriter.newLine();
            if (startPos > endPos) break;
        }
        bufferedWriter.close();
    }
}
